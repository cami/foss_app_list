# About

This repository contains some great opensource apps.

This project started in an Off-Topic group from different members of the community. We often had people who ask for a list of opensource apps we'd recommend.

So we had the idea to create a list with opensource apps which others also can contribute.

We did not test any of these apps regarding possible security vulnarabities neither have we given any  privacy grading for these apps. At the end this is a list from the community about which opensource apps they like.

For some services there are multiple possible solutions. We decided to have multiple solutions for one service as the end user can decide which solution for himself.

# List of opensource apps

## Web Browsers

| Application Name           | Description | Source Code                            |
| -------------------------- | ----------- | -------------------------------------- |
| Bromite                    |             | https://github.com/bromite/bromite     |
| Brave                      |             | https://github.com/brave/brave-browser |
| Fennec                     |             |
| Privacy Browser / Stoutner |             |
Iceraven

## Keyboards

| Application Name | Description | Source Code                                        |
| ---------------- | ----------- | -------------------------------------------------- |
| Simple Keyboard  |             | https://github.com/rkkr/simple-keyboard            |
| OpenBoard        |             | https://github.com/dslul/openboard                 |
| AnySoftKeyboard  |             | https://github.com/AnySoftKeyboard/AnySoftKeyboard |
FlorisBoard

## Video Players

| Application Name | Description | Source Code |
| ---------------- | ----------- | ----------- |
| VLC              |             |
| Nova             |             |
| Kodi             |             |
| Metro            |             |             |
| Simple Music Player|           |             |

## Communication

| Application Name | Description | Source Code                                           |
| ---------------- | ----------- | ----------------------------------------------------- |
| Signal           |             | https://github.com/signalapp/Signal-Android           |
| Element          |             | https://github.com/vector-im/element-android          |
| Briar            |             | https://code.briarproject.org/briar/briar/tree/master |
| Jami             |             | https://git.jami.net/savoirfairelinux/ring-project    |

## Image Viewer

| Application Name   | Description | Source Code                                         |
| ------------------ | ----------- | --------------------------------------------------- |
| Simple Gallery Pro |             | https://github.com/SimpleMobileTools/Simple-Gallery |

## Social Media

### Facebook

| Application Name   | Description | Source Code                                      |
| ------------------ | ----------- | ------------------------------------------------ |
| Frost for Facebook |             | https://github.com/AllanWang/Frost-for-Facebook/ |

### Reddit

| Application Name    | Description | Source Code                                |
| ------------------- | ----------- | ------------------------------------------ |
| Slide               |             | https://github.com/ccrama/Slide            |
| RedReader           |             | https://github.com/QuantumBadger/RedReader |
| Infinity for Reddit |             |

### Youtube

| Application Name | Description | Source Code                             |
| ---------------- | ----------- | --------------------------------------- |
| NewPipe          |             | https://github.com/TeamNewPipe/NewPipe/ |

## OTP / Token Authenticator

| Application Name | Description | Source Code                              |
| ---------------- | ----------- | ---------------------------------------- |
| Aegis            |             | https://github.com/beemdevelopment/Aegis |
| andOTP           |             | https://github.com/andOTP/andOTP         |

## Launchers

| Application Name    | Description | Source Code                                              |
| ------------------- | ----------- | -------------------------------------------------------- |
| KISS Launcher       |             | https://github.com/Neamar/KISS                           |
| OpenLauncher        |             | https://github.com/OpenLauncherTeam/openlauncher         |
| Simple App Launcher |             | https://github.com/SimpleMobileTools/Simple-App-Launcher |
| posidon launcher    |             | https://github.com/lposidon/posidonLauncher              |

## Feed Readers

| Application Name | Description | Source Code                           |
| ---------------- | ----------- | ------------------------------------- |
| Flym             |             | https://github.com/FredJul/Flym       |
| Feeder           |             | https://gitlab.com/spacecowboy/Feeder |

## PDF Readers

| Application Name | Description | Source Code                              |
| ---------------- | ----------- | ---------------------------------------- |
| Librera Pro      |             | https://github.com/foobnix/LibreraReader |
| MuPDF            |             | https://git.ghostscript.com/mupdf.git/   |

## Adblocker

| Application Name  | Description | Source Code                                   |
| ----------------- | ----------- | --------------------------------------------- |
| Blokada           |             | https://github.com/blokadaorg/fem             |
| personalDNSfilter |             | https://github.com/IngoZenz/personaldnsfilter |
| NetGuard          |             | https://github.com/M66B/NetGuard/             |
| Nebulo            |             |

## Utilities

| Application Name | Description                                                    | Source Code                                           |
| ---------------- | -------------------------------------------------------------- | ----------------------------------------------------- |
| Net Monitor      |                                                                | https://github.com/SecUSo/privacy-friendly-netmonitor |
| EteSync          |                                                                | https://github.com/etesync/android                    |
| DAVx⁵            |                                                                | https://gitlab.com/bitfireAT/davx5-ose                |
| Standard Notes   |                                                                |
| Syncthing        |                                                                | https://github.com/syncthing/syncthing-android        |
| Auto Auto-Rotate | Automatically remembers your auto-rotate setting for every app | https://gitlab.com/juanitobananas/auto-auto-rotate    |
| TrackerControl   |                                                                | https://github.com/OxfordHCC/tracker-control-android  |
